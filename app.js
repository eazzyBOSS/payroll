const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const passport = require('passport');
const OrganizationModel = require('./Models/Organization');
const app = express();

// Connect to DB
mongoose.connect(`mongodb://eazzyLEE:payroll123@ds213968.mlab.com:13968/payroll`, { autoIndex: false, useNewUrlParser: true })
  .then(() => console.log(`Hey Eazzy!, your MongoDB is now connected...`))
  .catch(err => console.log(err));

require('./authMiddleware/auth');

const routes = require('./api/v1/routes');
const privateRoutes = require('./api/v1/private_routes');
const port = process.env.PORT || 7000;

// Body parser middleware
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

// Passport middleware
app.use(passport.initialize())


// Use Routes
app.use('/api/v1', routes);
app.use('/api/v1/org', passport.authenticate('jwt', { session : false }), privateRoutes );

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({ error : err });
  });

app.listen(port, () => {
    console.log(`Hey Eazzy!, your Server has started on port ${port}`);
});