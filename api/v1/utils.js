require("dotenv").config();
const nodemailer = require("nodemailer");
const sendgridTransport = require("nodemailer-sendgrid-transport");
const Nexmo = require("nexmo");

const nexmo = new Nexmo({
  apiKey: process.env.NEXMO_API_KEY,
  apiSecret: process.env.NEXMO_API_SECRET
});

const transporter = nodemailer.createTransport(
  sendgridTransport({
    auth: {
      api_user: process.env.SG_USER, // SG username
      api_key: process.env.SG_PASS // SG password
    }
  })
);

const sendMail = (email, token) => {
  const message = {
    from: '"PayEazy 👻" <service@payeazy.com>', // Sender address
    to: email, // List of recipients
    subject: "Hello!! Subscription Notice ✔", // Subject line
    html: `<b>Hello there! Thank you for subscribing to our Payroll company services!</b><br></br><p style="font-family: Sarabun">Click on this link to verify your email <a href="${process.env.HOST_URL}/api/v1/company_verification?token=${token}">Company Account Confirmation Link</a>  ${process.env.HOST_URL}/api/v1/company_verification?token=${token}</p><br></br><p>We are here to serve you!</p><br></br><p>From all of us at <i>PayEazy</i></p><br></br><p style="color: #3399ff; font-family: Sarabun">Contact us at <a href="mailto:support@payeazy.com"><i>support@payeazy.com</i></a></p>`
  };

  transporter.sendMail(message, function(err, info) {
    console.log(process.env);
    if (err) {
      console.log(err);
    } else {
      console.log(info);
      console.log("Message sent: %s", info.messageId);
      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    }
  });
};

const sendOtp = otp => {
  const from = "Payeazy";
  const to = "2348100473702";
  const text = `Here is your one time password for your Payeazy account: ${otp}\r\n -The Payeazy team...`;
  // "You have successfully subscribed to Payeazy's payroll service! The Payeazy team...";

  nexmo.message.sendSms(from, to, text, (err, responseData) => {
    if (err) {
      console.log(err);
    } else {
      if (responseData.messages[0]["status"] === "0") {
        console.log("Message sent successfully.");
      } else {
        console.log(
          `Message failed with error: ${responseData.messages[0]["error-text"]}`
        );
        return false;
      }
    }
  });
};

module.exports = { sendMail, sendOtp };
