const express = require("express");
const EmployeeModel = require("../../Models/Employee");
const router = express.Router();

const { getProfile } = require("../v1/company/controller");
const {
  signUpEmployee,
  fetchEmployee,
  fetchEmployees
} = require("../v1/employee/controller");

// GET COMPANY PROFILE
router.get("/profile", getProfile);

// SIGNUP STAFF
router.post("/employee_create", signUpEmployee);

// FETCH EMPLOYEE PROFILE
router.get("/employee/:id", fetchEmployee);

// FETCH ALL EMPLOYEES BY COMPANY
router.get("/employees", fetchEmployees);

module.exports = router;
