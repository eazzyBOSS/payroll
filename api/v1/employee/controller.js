const express = require("express");
const EmployeeModel = require("../../../Models/Employee");

// @route   POST api/v1/org/employee_create
// @desc    Register company employee
// @access  Private
// @params  req
const signUpEmployee = async (req, res) => {
  const first_name = req.body.first_name;
  const last_name = req.body.last_name;
  const email = req.body.email;
  const identification_number = req.body.identification_number;
  const address = req.body.address;
  const date_of_birth = req.body.date_of_birth;
  const phone_number = req.body.phone_number;
  const account_number = req.body.account_number;
  const bank_name = req.body.bank_name;
  const company = req.user._id;

  try {
    //Save the information provided by the organization to the the database
    const employee = await EmployeeModel.create({
      email,
      first_name,
      last_name,
      identification_number,
      address,
      date_of_birth,
      phone_number,
      account_number,
      bank_name,
      company
    });

    //Send the employee information to the next middleware
    return res.send({
      message: "Employee Profile successfully created",
      employee
      // employees
    });
  } catch (error) {
    res.send({ message: "Failed Response", error });
  }
};

// @route   GET api/v1/org/employee
// @desc    Fetch company employee
// @access  Private
// @params  req
const fetchEmployee = async (req, res) => {
  try {
    //Fetch the information provided by the organization to the the database
    const employee = await EmployeeModel.findById(req.params.id);

    //Send the employee information to the next middleware
    return res.send({
      message: "Employee Found!",
      employee
    });
  } catch (error) {
    res.send({
      message: "Employee Not Found!",
      error
    });
  }
};

// @route   GET api/v1/org/employees
// @desc    Fetch all company employees
// @access  Private
// @params  req
const fetchEmployees = (req, res) => {
  const errors = {};
  console.log(req.user);
  //Fetchthe information provided by the organization to the the database
  EmployeeModel.find({ company: req.user._id })
    .populate("organization", ["name", "registration_number"])
    .then(employees => {
      if (!employees) {
        errors.noprofile = "There are no employees";
        return res.status(404).json(errors);
      }

      res.json(employees);
    })
    .catch(err => res.status(404).json({ profile: "There are no employees" }));
};

module.exports = { signUpEmployee, fetchEmployee, fetchEmployees };
