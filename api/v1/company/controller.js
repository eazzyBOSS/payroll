// @route   GET api/v1/org/profile
// @desc    Get Company Profile
// @access  Private
// @params  req
const getProfile = (req, res) => {
  res.json({
    message: "Company Profile- OK",
    user: req.user,
    token: req.headers.authorization.split(" ")[1]
  });
};

module.exports = { getProfile };
