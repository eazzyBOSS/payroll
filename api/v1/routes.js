const express = require("express");
const router = express.Router();
const { signUp, logIn, verifyEmail, verifyOtp } = require("./auth/controller");

router.post("/signup", signUp);
router.post("/login", logIn);
router.get("/company_verification", verifyEmail);
router.post("/otp_verification", verifyOtp);

module.exports = router;
