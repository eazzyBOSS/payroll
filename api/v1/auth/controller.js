const express = require("express");
const passport = require("passport");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");
const Organization = require("../../../Models/Organization");
const Token = require("../../../Models/Token");
const Otp = require("../../../Models/Otp");
const router = express.Router();
const { sendMail, sendOtp } = require("../utils");

// Load Input Validation
const validateRegisterInput = require("../../../validations/register");
// const validateLoginInput = require("../../validations/login");

// UNIQUE KEY GENERATOR
const uniqueKeyGen = name => {
  let id = "";
  let rand = Math.random()
    .toString()
    .replace(/[^0-9]+/g, "")
    .substr(0, 7);
  if (name.split(" ").length > 1) {
    id =
      name
        .split(" ")[0]
        .split("")[0]
        .toUpperCase() +
      name
        .split(" ")[1]
        .split("")[0]
        .toUpperCase();
  } else {
    id = name.split("")[0].toUpperCase() + name.split("")[1].toUpperCase();
  }
  return id + rand;
};

// @route   POST api/v1/signup
// @desc    Register company
// @access  Public
// @params  req
const signUp = (req, res) => {
  console.log("signup hit!");
  const { errors, isValid } = validateRegisterInput(req.body);
  console.log("2nd signup hit!");
  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  Organization.findOne({ email: req.body.email }).then(org => {
    if (org) {
      errors.email = "Email already exists";
      return res.status(400).json(errors);
    } else {
      console.log("hitting DB");
      const unique_key = uniqueKeyGen(req.body.company_name);
      const newOrganization = new Organization({
        company_name: req.body.company_name,
        email: req.body.email,
        address: req.body.address,
        unique_key,
        phone_number: req.body.phone_number,
        date_of_registration: req.body.date_of_registration,
        registration_number: req.body.registration_number,
        status: "pending",
        account_number: "",
        bank_name: "",
        key_contact: "",
        state: "",
        bank_verification_number: "",
        email_verification_status: "pending",
        phone_number_verification_status: "pending",
        bank_verification_number_status: "pending",
        account_number_verification_status: "pending"
      });

      const oneTimeToken = Math.random()
        .toString()
        .replace(/[^0-9]+/g, "")
        .substr(0, 6);

      newOrganization
        .save()
        .then(org => {
          const newOtp = new Otp({
            _companyId: newOrganization._id,
            otp: oneTimeToken
          });

          newOtp
            .save()
            .then(response => {
              sendOtp(newOtp.otp);
              // sendMail(req.body.email, newOtp.token);
              res.json({ data: org });
            })
            .catch(error => {
              console.log(error);
              res.json({
                message:
                  "There is most likely something wrong with the server. Please try again...",
                error: error
              });
            });
        })
        .catch(err => {
          console.log(err);
          res.json({
            error:
              "There is most likely something wrong with the server. Please try again..."
          });
        });
    }
  });
};

// @route   POST api/v1/login
// @desc    Company Login
// @access  Public
// @params  req
const logIn = (req, res, next) => {
  passport.authenticate("login", async (err, user, info) => {
    try {
      if (err || !user) {
        return res.status(400).json({ error: info }); //next(error);
      }
      req.login(user, { session: false }, async error => {
        if (error) return next(error);

        const body = {
          _id: user._id,
          email: user.email,
          unique_key: user.unique_key
        };
        //Sign the JWT token and populate the payload with the user email and id
        const token = jwt.sign({ user: body }, "top_secret");
        //Send back the token to the user
        return res.json({ user: user, token: token });
      });
    } catch (error) {
      return res.status("400").json({ error: "something went wrong" }); //next(error);
    }
  })(req, res, next);
};

// @route   GET api/v1/company_verification
// @desc    Verify company email address
// @access  Public
// @query   token
const verifyEmail = (req, res) => {
  try {
    console.log(req.query.token);
    //Fetch the token from url query and cross-cehck against DB
    const token = Token.findOne({ token: req.query.token });
    token
      .then(response => {
        if (!response) {
          return res.status(400).send({
            type: "not-verified",
            message:
              "We were unable to find a valid token. Your token my have expired."
          });
        }
        Organization.findOne({ _id: response._companyId })
          .then(result => {
            if (!result)
              return res.status(400).send({
                message: "We were unable to find a user for this token."
              });
            if (result.email_verification_status === "verified")
              return res.status(400).send({
                type: "already-verified",
                message: "This user has already been verified."
              });

            // Verify and update company data
            result.email_verification_status = "verified";
            result.save(function(err) {
              if (err) {
                return res.status(500).send({ message: err.message });
              }
              res.status(200).send({
                message: "The account has been verified. Please verify.",
                data: result
              });
            });
          })
          .catch(err => {
            console.log(err);
          });
      })
      .catch(error => {
        return res.status(404).json({ message: "This token is expired..." });
      });
  } catch (error) {
    res.send({
      message: "Somethings is wrong.. Please try again",
      error
    });
  }
};

// @route   POST api/v1/otp_verification
// @desc    Verify company mobile number
// @access  Public
// @query   token
const verifyOtp = (req, res) => {
  try {
    console.log(req.body.otp);
    //Fetch the token from request body and cross-cehck against DB
    const otp = Otp.findOne({ otp: req.body.otp });
    otp
      .then(response => {
        if (!response) {
          return res.status(400).send({
            type: "not-verified",
            message:
              "We were unable to find a valid one-time token. Your token my have expired."
          });
        }
        Organization.findOne({
          _id: response._companyId,
          email: req.body.email
        })
          .then(result => {
            if (!result)
              return res.status(400).send({
                message:
                  "We were unable to find an organization with this token."
              });
            if (result.phone_number_verification_status === "verified")
              return res.status(400).send({
                type: "already-verified",
                message: "This mobile number has already been verified."
              });

            // Verify and update company data
            result.phone_number_verification_status = "verified";
            result.save(function(err) {
              if (err) {
                return res.status(500).send({ message: err.message });
              }

              // Create Token for Email
              const newToken = new Token({
                _companyId: response._id,
                token: crypto.randomBytes(16).toString("hex")
              });

              newToken
                .save()
                .then(rep => {
                  sendMail(result.email, newToken.token);
                  res.status(200).send({
                    message:
                      "Your mobile number has been verified. Please log in.",
                    data: rep
                  });
                })
                .catch(error => {
                  res.status(403).json({
                    error:
                      "There is most likely something wrong with the server at this time. Please try again..."
                  });
                });
            });
          })
          .catch(err => {
            console.log(err);
          });
      })
      .catch(error => {
        return res.status(404).json({ message: "This token is expired..." });
      });
  } catch (error) {
    res.send({
      message: "Somethings is wrong.. Please try again",
      error
    });
  }
};

module.exports = { signUp, logIn, verifyEmail, verifyOtp };
