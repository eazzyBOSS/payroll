const mysql = require('mysql');

// CREATE mysql CONNECTION
const mydb = mysql.createConnection({
    host: 'http://192.168.64.2',
    user: 'root',
    password: 'password',
    database: 'payroll',
})


// CONNECT DB
mydb.connect((error) => {
    if(error) {
        console.log('oops!');
        throw error;
    }
    console.log('Hey Eazzy!, your DB is now connected...');
});