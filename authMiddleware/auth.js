const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const OrganizationModel = require('../Models/Organization');

//Create a passport middleware to handle user registration
passport.use('signup', new localStrategy({
  usernameField : 'email',
  passwordField : 'password',
  passReqToCallback: true
}, async (req, email, password, done) => {
  const name = req.body.name,
    registration_number = req.body.registration_number,
    address = req.body.address,
    date_of_registration = req.body.date_of_registration,
    status = "pending"

    try {
      //Save the information provided by the organization to the the database
      const organization = await OrganizationModel.create({ email, password, name, registration_number, address, date_of_registration, status });

      //Send the organization information to the next middleware
      return done(null, organization);
    } catch (error) {
      done(error);
    }
}));

//Create a passport middleware to handle User login
passport.use('login', new localStrategy({
  usernameField : 'email',
  passwordField : 'unique_key'
}, async (email, unique_key, done) => {
  try {
    //Find the organization associated with the email provided by the organization
    const organization = await OrganizationModel.findOne({ email });
    if( !organization ){
      //If the organization isn't found in the database, return a message
      return done(null, false, { message : 'Organization not found'});
    }
    //Validate password and make sure it matches with the corresponding hash stored in the database
    //If the passwords match, it returns a value of true.
    // const validate = await organization.isValidPassword(password);
    // if( !validate ){
    //   return done(null, false, { message : 'Wrong Password'});
    // }
    const validateUniqueKey = await organization.isValidUniqueKey(unique_key);
    if(!validateUniqueKey) {
      return done(null, false, { message : 'Wrong Identifier'});
    }
    //Send the organization information to the next middleware
    return done(null, organization, { message : 'Logged in Successfully'});
  } catch (error) {
    return done(error);
  }
}));

const JWTstrategy = require('passport-jwt').Strategy;
//We use this to extract the JWT sent by the user
const ExtractJWT = require('passport-jwt').ExtractJwt;

passport.use(new JWTstrategy({
  secretOrKey : 'top_secret',
  jwtFromRequest : ExtractJWT.fromAuthHeaderAsBearerToken()
  // jwtFromRequest : ExtractJWT.fromUrlQueryParameter('secret_token')
}, async (token, done) => {
  try {
    return done(null, token.user);
  } catch (error) {
    done(error);
  }
}));