const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.company_name = !isEmpty(data.company_name) ? data.company_name : "";
  data.email = !isEmpty(data.email) ? data.email : "";
  data.address = !isEmpty(data.address) ? data.address : "";
  data.phone_number = !isEmpty(data.phone_number) ? data.phone_number : "";
  data.registration_number = !isEmpty(data.registration_number)
    ? data.registration_number
    : "";

  if (!Validator.isLength(data.company_name, { min: 2, max: 30 })) {
    errors.company_name = "Company Name must be between 2 and 30 characters";
  }

  if (!Validator.isLength(data.phone_number, { min: 11, max: 11 })) {
    errors.phone_number = "Phone Number must be 11 characters";
  }

  if (Validator.isEmpty(data.company_name)) {
    errors.company_name = "Company Name field is required";
  }

  if (Validator.isEmpty(data.email)) {
    errors.email = "Email field is required";
  }

  if (!Validator.isEmail(data.email)) {
    errors.email = "Email is invalid";
  }

  if (Validator.isEmpty(data.address)) {
    errors.address = "Address field is required";
  }

  if (Validator.isEmpty(data.phone_number)) {
    errors.phone_number = "Phone Number field is required";
  }

  if (Validator.isEmpty(data.registration_number)) {
    errors.registration_number = "Registration Number field is required";
  }

  // if (Validator.isEmpty(data.password)) {
  //   errors.password = "Password field is required";
  // }

  // if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
  //   errors.password = "Password must be at least 6 characters";
  // }

  // if (Validator.isEmpty(data.password2)) {
  //   errors.password2 = "Confirm Password field is required";
  // }

  // if (!Validator.equals(data.password, data.password2)) {
  //   errors.password2 = "Passwords must match";
  // }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
