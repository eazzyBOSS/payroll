const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

const OrganizationSchema = new Schema({
  company_name: {
    type: String,
    required: true,
    unique: true
  },
  registration_number: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  unique_key: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  date_of_registration: {
    type: String,
    required: true
  },
  account_number: {
    type: String
  },
  bank_name: {
    type: String
  },
  key_contact: {
    type: String
  },
  state: {
    type: String
  },
  phone_number: {
    type: String,
    required: true
  },
  bank_verification_number: {
    type: String
  },
  email_verification_status: {
    type: String
  },
  phone_number_verification_status: {
    type: String
  },
  bank_verification_number_status: {
    type: String
  },
  account_number_verification_status: {
    type: String
  },
  status: {
    type: String,
    required: true
  },
  penalty_types: {
    type: Object
  }
});

// OrganizationSchema.pre('save', async function(next){
//     //'this' refers to the current document about to be saved
//     const organization = this;
//     //Hash the password with a salt round of 10, the higher the rounds the more secure, but the slower
//     //your application becomes.
//     const hash = await bcrypt.hash(this.password, 10);
//     //Replace the plain text password with the hash and then store it
//     this.password = hash;
//     //Indicates we're done and moves on to the next middleware
//     next();
//   });

//   //We'll use this later on to make sure that the user trying to log in has the correct credentials
//   OrganizationSchema.methods.isValidPassword = async function(password){
//     const organization = this;
//     //Hashes the password sent by the user for login and checks if the hashed password stored in the
//     //database matches the one sent. Returns true if it does else false.
//     const compare = await bcrypt.compare(password, organization.password);
//     return compare;
//   }
OrganizationSchema.methods.isValidUniqueKey = async function(uniqueKey) {
  const organization = this;

  if (uniqueKey === organization.unique_key) {
    return true;
  } else {
    return false;
  }
};

const OrganizationModel = mongoose.model("organization", OrganizationSchema);

module.exports = OrganizationModel;
