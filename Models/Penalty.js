const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const PenaltySchema = new Schema({
  employee: {
    type: Schema.Types.ObjectId,
    ref: 'employee'
  },
  penaltyId: {
    type: Schema.Types.ObjectId,
    ref: 'penaltyType'
  },
  amount: {
    type: Number
  }
});

const PenaltyModel = mongoose.model('penalty', PenaltySchema);
  
module.exports = PenaltyModel;