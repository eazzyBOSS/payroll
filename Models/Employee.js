const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EmployeeSchema = new Schema({
  company: {
    type: Schema.Types.ObjectId,
    ref: 'organization'
  },
  identification_number: {
    type: String
  },
  first_name: {
    type: String,
    required: true
  },
  last_name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  address: {
    type: String,
    required: true
  },
  phone_number: {
    type: Number,
    required: true
  },
  date_of_birth: {
    type: Date
  },
  account_number: {
    type: Number,
    required: true,
  },
  bank_name: {
    type: String,
    required: true,
  },
  gross_salary: {
    type: Number
  },
  net_salary: {
    type: Number
  },
  tax: {
    type: Number
  },
  start_date: {
    type: Date
  }
})

const EmployeeModel = mongoose.model('employee', EmployeeSchema);
  
module.exports = EmployeeModel;