const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const OtpSchema = new Schema({
  _companyId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "organization"
  },
  otp: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    required: true,
    default: Date.now,
    expires: 300
  }
});

const OtpModel = mongoose.model("otp", OtpSchema);

module.exports = OtpModel;
