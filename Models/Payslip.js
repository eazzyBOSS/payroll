const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const PayslipSchema = new Schema({
  company: {
    type: Schema.Types.ObjectId,
    ref: 'employee'
  },
  amount: {
    type: Number
  },
  payment_date: {
    type: Number
  },
  remarks: {
    type: String
  },
  totalDeductions: {
    type: Number
  },
  penalties: {
    type: Number
  },
  tax: {
    type: Number
  }
});

const PayslipModel = mongoose.model('payslip', PayslipSchema);
  
module.exports = PayslipModel;