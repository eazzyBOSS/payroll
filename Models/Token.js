const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TokenSchema = new Schema({
  _companyId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "organization"
  },
  token: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    required: true,
    default: Date.now,
    expires: 43200
  }
});

const TokenModel = mongoose.model("token", TokenSchema);

module.exports = TokenModel;
