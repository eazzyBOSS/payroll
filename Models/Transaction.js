const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const TransactionSchema = new Schema({
  company: {
    type: Schema.Types.ObjectId,
    ref: 'company'
  },
  employee: {
    type: Schema.Types.ObjectId,
    ref: 'employee'
  },
  referenceNumber: {
    type: Number
  },
  remark: {
    type: String
  },
  channel: {
    type: String
  },
  amount: {
    type: Number
  },
  charge: {
    type: String
  }
});

const TransactionModel = mongoose.model('transaction', TransactionSchema);
  
module.exports = TransactionModel;