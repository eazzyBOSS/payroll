module.exports = {
    "up": "CREATE TABLE organizations (organization_id INT AUTO_INCREMENT, UNIQUE KEY organization_id (organization_id), name TEXT(350), registration_number INT(50), address TEXT(255), date_of_registration DATE, account_number INT(12) )",
    "down": "DROP TABLE organizations"
}