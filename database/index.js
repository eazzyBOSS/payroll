const mysql = require('mysql');
const mongoose = require('mongoose');

// CREATE mysql CONNECTION
const mydb = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'payroll',
})

module.exports = mydb;